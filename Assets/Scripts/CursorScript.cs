﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorScript : MonoBehaviour
{
    private Vector3 _mDefaultPosition;
    private GameObject _mCollidingObject;

    private bool _mMoving = false;

    void Start()
    {
        _mDefaultPosition = gameObject.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit lHit;

        Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out lHit);

        if (lHit.collider != null)
        {
            gameObject.transform.position = lHit.point;

            if (_mMoving)
            {
                _mCollidingObject.transform.position = lHit.point;
                Debug.Log("HIT! " + _mCollidingObject.name);
            }
            else
            {
                _mCollidingObject = lHit.collider.gameObject;
            }
        }
        else
        {
            Debug.Log("MISS!");
            gameObject.transform.localPosition = _mDefaultPosition;

            if (_mMoving)
            {
                _mCollidingObject.transform.position = gameObject.transform.position;
            }
            else
            {
                _mCollidingObject = null;
            }
        }

        if (_mCollidingObject != null)
        {
            if (Input.touchCount > 0)
            {
                _mMoving = true;

                _mCollidingObject.GetComponent<Collider>().enabled = false;
            }
            else
            {
                _mMoving = false;

                _mCollidingObject.GetComponent<Collider>().enabled = true;
            }
        }
    }
}
